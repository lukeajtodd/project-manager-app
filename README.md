# Project Manager

## Features:

- [ ] Tasks
- [ ] Issues
- [ ] Time-tracking

## Integrations:
- [ ] Gitlab
- [ ] Bitbucket
- [ ] Github

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


## Apollo

- Start the GraphQL API Server with `yarn run apollo`
- INFO  apollo  Customize the mocks in `apollo-server/mocks.js`